# My conky clock

I rewrite a simple conky clock. 

## screenshot

![clock](screenshot/clock.png)

## environment

- lua: 5.1.5
- conky: 1.10.8

For gentoo, you should enable the imlib use of conky.
